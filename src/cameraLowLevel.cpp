#include "cameraLowLevel.h"

#include <sys/ioctl.h>
#include <errno.h>


int CameraLowLevel::ioctlCommand( int fh, int request, void* arg )
{
  int res;
  
  do {
    res = ioctl( fh, request, arg );
  } while ( res == -1 && errno == EINTR );

  return res;
}
