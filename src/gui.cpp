#include "gui.h"
#include "config.h"

#include <glibmm/main.h>
#include <gtkmm.h>

#include <cstdlib>


Gui::Gui( Image* img1, ThresholdImage* img2, CameraSettings* camSettings, volatile NewImageRequest* newImgReq, Config* config_ )
  : image( img1 ),
    thresImage( img2 ),
    newImgRequired( newImgReq ),
    config( config_ ),
    cameraSettings( camSettings ),
    halt( false )
{
  // Artificial command line arguements
  // Application class has a create method without them that should result in default behaviour,
  // but for some reason it crashes the application. That's why I have a little hack here.
  int argc = 1;
  const char* container[] = { "vision_conf" };
  char** argv = (char**) &container[0];
  
  application = Gtk::Application::create( argc, argv, "robotex.vision_conf" );

  // Creating all the necessary windows
  mainWindow = new MainWindow();
  mainWindow->move( config->mainWnd_x, config->mainWnd_y );
  confWindow = new ConfWindow( camSettings, newImgReq );
  confWindow->move( config->confWnd_x, config->confWnd_y );
  videoWindow = new DisplayWindow( "Video", image->width, image->height, config->wnd1_x, config->wnd1_y );
  thresWindow = new DisplayWindow( "Threshold", thresImage->width, thresImage->height, config->wnd2_x, config->wnd2_y );

  videoWindow->setImage( img1 );
  thresWindow->setImage( img2 );
  
  // Registering signal handlers
  videoWindow->setButtonPressHandler( sigc::mem_fun(img2, &ThresholdImage::addToThreshold) );
  thresWindow->setButtonPressHandler( sigc::mem_fun(img2, &ThresholdImage::removeFromThreshold) );
  videoWindow->setKeyPressHandler( sigc::mem_fun(this, &Gui::onKeyPress) );
  thresWindow->setKeyPressHandler( sigc::mem_fun(this, &Gui::onKeyPress) );
  
  mainWindow->colorComboBox.signal_changed().connect( sigc::mem_fun(*this, &Gui::onColorChange) );
  
  mainWindow->frameBrushEntry.signal_changed().connect( sigc::mem_fun(*this, &Gui::onFrameBrushChange) );
  mainWindow->colorBrushEntry.signal_changed().connect( sigc::mem_fun(*this, &Gui::onColorBrushChange) );
  mainWindow->thresFrameBrushEntry.signal_changed().connect( sigc::mem_fun(*this, &Gui::onThresFrameBrushChange) );
  mainWindow->thresColorBrushEntry.signal_changed().connect( sigc::mem_fun(*this, &Gui::onThresColorBrushChange) );
  
  mainWindow->resetButton.signal_clicked().connect( sigc::mem_fun(img2, &ThresholdImage::resetThreshold) );
  mainWindow->confButton.signal_clicked().connect( sigc::mem_fun(*this, &Gui::showConfWindow) );
  
  
  onColorChange();		// For setting initial values
  onFrameBrushChange();
  onColorBrushChange();
  onThresFrameBrushChange();
  onThresColorBrushChange();
  
  
  // Setting an idle function for the mainWindow
  Glib::signal_idle().connect( sigc::mem_fun(*this, &Gui::onIdle) );
  
  running = true;
  exitCode = 0;
}


Gui::~Gui()
{
  delete mainWindow;
  delete confWindow;
  delete videoWindow;
  delete thresWindow;
}


void Gui::run()
{
  if( !videoWindow->displayReady ) {
    printf( "Error. Video Window initialization failed.\n" );
    exitCode = -1;
    return;
  } else if( !thresWindow->displayReady ) {
    printf( "Error. Threshold Window initialization failed.\n" );
    exitCode = -1;
    return;
  }
  
  application->run( *mainWindow );
  closeApplication();
  
  return;
}



bool Gui::onIdle()
{
  if (!videoWindow->displayReady || !thresWindow->displayReady) {
    closeApplication();
    return false;
  }
  
  if (*newImgRequired == NoRequest) {
    videoWindow->run();
    thresWindow->run();
  } else if (*newImgRequired == ResizeWindowRequest) {
    resizeDisplayWindows( image->width, image->height );
    
    // Restarting the conf window to display the new set of options
    confWindow->hide();
    delete confWindow;
    confWindow = new ConfWindow( cameraSettings, newImgRequired );
    confWindow->move( config->confWnd_x, config->confWnd_y );
    confWindow->show();
    
    *newImgRequired = NoRequest;
  }

  
  // Saving window positions to make the UI more friendly
  mainWindow->get_position( config->mainWnd_x, config->mainWnd_y );
  if( confWindow->get_visible() )
    confWindow->get_position( config->confWnd_x, config->confWnd_y );
  videoWindow->getPosition( config->wnd1_x, config->wnd1_y );
  thresWindow->getPosition( config->wnd2_x, config->wnd2_y );
  
  return true;
}



void Gui::showConfWindow()
{
  confWindow->show();
}



void Gui::closeApplication()
{
  application->quit();
  
  videoWindow->destroyWindow();
  thresWindow->destroyWindow();
  
  running = false;
}



void Gui::lockDisplay()
{
  videoWindow->lockDisplay();
  thresWindow->lockDisplay();
}


void Gui::unLockDisplay()
{
  videoWindow->unLockDisplay();
  thresWindow->unLockDisplay();
}



void Gui::setNewImage( Image* img )
{
  videoWindow->img = img;
}

void Gui::setNewThresImage( ThresholdImage* img )
{
  thresWindow->img = img;
}


void Gui::resizeDisplayWindows( int width, int height )
{
  videoWindow->resizeWindow( width, height );
  thresWindow->resizeWindow( width, height );
}




void Gui::onKeyPress( int key )
{  
  if (key == 65)	// Space
    halt = !halt;
  else
    thresImage->threshold->resetToBackUp();
}


void Gui::onColorChange()
{
  int id = mainWindow->colorComboBox.get_active_row_number();
  thresImage->setThresColor( id + 1 );
}


void Gui::onFrameBrushChange() {
  int val = atoi( mainWindow->frameBrushEntry.get_text().c_str() );
  if( val > 0 ) val--;
  thresImage->setAddThresRadius( val );
}


void Gui::onColorBrushChange() {
  int val = atoi( mainWindow->colorBrushEntry.get_text().c_str() );
  if( val > 0 ) val--;
  thresImage->threshold->setAddColorBrush( val );
}


void Gui::onThresFrameBrushChange() {
  int val = atoi( mainWindow->thresFrameBrushEntry.get_text().c_str() );
  if( val > 0 ) val--;
  thresImage->setRemoveThresRadius( val );
}


void Gui::onThresColorBrushChange() {
  int val = atoi( mainWindow->thresColorBrushEntry.get_text().c_str() );
  if( val > 0 ) val--;
  thresImage->threshold->setRemoveColorBrush( val );
}








