#ifndef DUMMY_THRES_IMAGE_H_INCLUDED
#define DUMMY_THRES_IMAGE_H_INCLUDED


#include "thresholdImage.h"


class DummyThresImage : public ThresholdImage
{
public:
  DummyThresImage( int width, int height, Threshold* thres ) : ThresholdImage( width, height, thres ) {}
  virtual ~DummyThresImage( void ) {}
  
  inline bool thresholdColor( uchar& col1, uchar& col2, uchar& col3, uchar color ) {
    return false;
  }
  
  inline void addToThreshold( int xCoord, int yCoord ) {}
  inline void removeFromThreshold( int xCoord, int yCoord ) {}
  inline void resetThreshold( void ) {}
  
  inline void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) {
    r = 0;
    g = 0;
    b = 0;
  }
};


#endif