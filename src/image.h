#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED


typedef unsigned char uchar;


class Image
{
public:
  Image( int width_, int height_ )
    : width( width_ ),
      height( height_ ),
      displayed( false ),
      frame( NULL )
  {}
  
  virtual ~Image( void ) {}
  
  inline bool isFrameSet() {
    return frame != NULL;
  }
  
  inline void setFrame( uchar* frame_ ) {
    frame = frame_;
    displayed = false;
  }
  
  inline void removeFrame( void ) {
    frame = NULL;
  }
  
  virtual void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) = 0;
  
public:
  int width;
  int height;
  volatile bool displayed;
  
  
protected:
  uchar* frame;
};


#endif