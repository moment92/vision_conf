#ifndef DUMMY_IMAGE_H_INCLUDED
#define DUMMY_IMAGE_H_INCLUDED


#include "image.h"


class DummyImage : public Image
{
public:
  DummyImage( int width, int height ) : Image( width, height ) {}
  
  inline void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) {
    r = 0;
    g = 0;
    b = 0;
  }
};


#endif