#ifndef THRESHOLD_IMAGE_H_INCLUDED
#define THRESHOLD_IMAGE_H_INCLUDED

#include "image.h"
#include "threshold.h"


class ThresholdImage : public Image
{
public:
  ThresholdImage( int width, int height, Threshold* thres )
    : Image( width, height ),
      threshold( thres ),
      thresholdByColor( 1 ),
      addThresRadius( 0 ),
      removeThresRadius( 0 )
  {}
  
  virtual bool thresholdColor( uchar& col1, uchar& col2, uchar& col3, uchar color ) = 0;
  virtual void addToThreshold( int xCoord, int yCoord ) = 0;
  virtual void removeFromThreshold( int xCoord, int yCoord ) = 0;
  virtual void resetThreshold( void ) = 0;
  
  
  inline void setThresColor( uchar color ) {
    thresholdByColor = color;
  }
  
  inline void setAddThresRadius( int radius ) {
    addThresRadius = radius;
  }
  
  inline void setRemoveThresRadius( int radius ) {
    removeThresRadius = radius;
  }

  
public:
  Threshold* threshold;

  
protected:
  uchar thresholdByColor;
  int addThresRadius;
  int removeThresRadius;
};


#endif