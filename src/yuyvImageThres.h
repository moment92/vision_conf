#ifndef YUYV_IMAGE_THRES_H_INCLUDED
#define YUYV_IMAGE_THRES_H_INCLUDED


#include <algorithm>

#include "thresholdImage.h"


class YuyvImageThres : public ThresholdImage
{
public:
  YuyvImageThres( int width_, int height_, Threshold* thres )
    : ThresholdImage( width_, height_, thres )
    {}
  
  inline void getYUV( int xCoord, int yCoord, uchar& y, uchar& u, uchar& v ) {
    int tempX = xCoord - (xCoord % 2);
    int index = ((yCoord * width) + tempX) * 2;

    u = frame[index + 1];
    v = frame[index + 3];
    if (xCoord % 2 == 1)
      y = frame[index + 2];
    else
      y = frame[index];
  }
  
  inline void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) {
    uchar y, u, v;
    
    getYUV( xCoord, yCoord, y, u, v );
    if (thresholdColor(y, u, v, thresholdByColor))
      r = g = b = 255;
    else
      r = g = b = 0;
  }
  
  
  inline bool thresholdColor( uchar& y, uchar& u, uchar& v, uchar thresBit ) {
    return threshold->isThresholdColor( y, u, v, thresBit );
  }
  
  
  inline void addToThreshold( int xCoord, int yCoord ) {
    uchar y, u, v;
    
    threshold->makeBackUp();
    
    for (int i = xCoord - addThresRadius; i <= xCoord + addThresRadius; i++) {
      if (i < 0 || i >= width)
	continue;
      for (int j = yCoord - addThresRadius; j <= yCoord + addThresRadius; j++) {
	if (j < 0 || j >= height)
	  continue;
	
	getYUV( i, j, y, u, v );
	threshold->assignColorRange( y, u, v, thresholdByColor );
      }
    }
  }
  
  
  inline void removeFromThreshold( int xCoord, int yCoord ) {
    uchar y, u, v;
    
    threshold->makeBackUp();
    
    for (int i = xCoord - removeThresRadius; i <= xCoord + removeThresRadius; i++) {
      if (i < 0 || i >= width)
	continue;
      for (int j = yCoord - removeThresRadius; j <= yCoord + removeThresRadius; j++) {
	if (j < 0 || j >= height)
	  continue;
	
	getYUV( i, j, y, u, v );
	threshold->removeColorRange( y, u, v, thresholdByColor );
      }
    }
  }
  
  inline void resetThreshold( void ) {
    threshold->makeBackUp(); 
    threshold->resetThreshold( thresholdByColor );
  }
};



#endif