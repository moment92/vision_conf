#ifndef YUYV_IMAGE_H_INCLUDED
#define YUYV_IMAGE_H_INCLUDED


#include "color.h"
#include "image.h"



class YuyvImage : public Image
{
public:
  
  YuyvImage( int width_, int height_ ) : Image( width_, height_ ) {}
  
  inline void getYUV( int xCoord, int yCoord, uchar& y, uchar& u, uchar& v ) {
    int tempX = xCoord - (xCoord % 2);
    int index = ((yCoord * width) + tempX) * 2;

    u = frame[index + 1];
    v = frame[index + 3];
    if (xCoord % 2 == 1)
      y = frame[index + 2];
    else
      y = frame[index];
  }
  
  inline void getRGB( int xCoord, int yCoord, uchar& r, uchar& g, uchar& b ) {
    uchar y, u, v;
    
    getYUV( xCoord, yCoord, y, u, v );
    Color::YuvToRgb( y, u, v, r, g, b );
  }

};


#endif