#include "confWindow.h"
#include "camera.h"

#include <linux/videodev2.h>
#include <gtkmm/scale.h>
#include <gtkmm/checkbutton.h>



ConfWindow::ConfWindow( CameraSettings* camSettings_, volatile NewImageRequest* newImgReq )
  : Window(),
    camSettings( camSettings_ ),
    newImageRequired( newImgReq )
{
  initWindow();
}



ConfWindow::~ConfWindow()
{
}


void ConfWindow::initWindow()
{
  set_size_request( 400, 200 );
  set_title( "Configure Camera" );
  
  createWindow();
  
  show_all_children();
}


void ConfWindow::createWindow()
{
  CameraSettings::SupportedFormat* currentFormat = NULL;
  CameraSettings::SupportedFormat::FormatSize* currentRes = NULL;
  int* currentFps = NULL;
  int index;
  
  layoutGrid.set_column_homogeneous( true );
  add( layoutGrid );
  
  fmtLabel.set_label( "   Pixel format" );
  layoutGrid.attach( fmtLabel, 0, 0, 1, 1 );
  resolutionLabel.set_label( "   Resolution" );
  layoutGrid.attach( resolutionLabel, 0, 1, 1, 1 );
  fpsLabel.set_label( "   Frame rate" );
  layoutGrid.attach( fpsLabel, 0, 2, 1, 1 );
  
  
  // Pixel formats
  for( int i = 0; i < camSettings->supportedFormats.size(); i++ ) {
    CameraSettings::SupportedFormat fmt = camSettings->supportedFormats[i];
    
    if( fmt.name == "YUYV" )
      pixelFmtComboBox.append( fmt.name );
    else
      pixelFmtComboBox.append( fmt.name + " (Not Implemented)" );
    
    if( fmt.name == camSettings->pixelFormat ) {
      currentFormat = &camSettings->supportedFormats[i];
      index = i;
    }
  }
  if( currentFormat == NULL ) {
    printf( "Corrupted camera settings.\n" );
    return;
  }
  pixelFmtComboBox.set_active( index );
  pixelFmtComboBox.signal_changed().connect( sigc::bind(sigc::mem_fun(*this, &ConfWindow::onFormatChange), &pixelFmtComboBox) );
  layoutGrid.attach( pixelFmtComboBox, 1, 0, 1, 1 );
  
  
  // Resolutions
  for( int i = 0; i < currentFormat->formatSizes.size(); i++ ) {
    CameraSettings::SupportedFormat::FormatSize fmtSize = currentFormat->formatSizes[i];
    resolutionComboBox.append( int2ustring(fmtSize.width) + " x " + int2ustring(fmtSize.height) );

    if( fmtSize.width == camSettings->width && fmtSize.height == camSettings->height ) {
      currentRes = &currentFormat->formatSizes[i];
      index = i;
    }
  }
  if( currentRes == NULL ) {
    printf( "Corrupted camera settings.\n" );
    return;
  }
  resolutionComboBox.set_active( index );
  resolutionComboBox.signal_changed().connect( sigc::bind(sigc::mem_fun(*this, &ConfWindow::onResolutionChange), &resolutionComboBox) );
  layoutGrid.attach( resolutionComboBox, 1, 1, 1, 1 );
  
  
  // Frame rates
  for( int i = 0; i < currentRes->fps.size(); i++ ) {
    fpsComboBox.append( int2ustring(currentRes->fps[i]) );
    
    if( currentRes->fps[i] == camSettings->fps ) {
      currentFps = &currentRes->fps[i];
      index = i;
    }
  }
  if( currentFps == NULL ) {
    // FPS options are likely to change after changing the pixel format or resolution, so the current one might not be listed
    // correct would be to ask the new fps, but this is a hack here
    index = 0;
  }
  fpsComboBox.set_active( index );
  fpsComboBox.signal_changed().connect( sigc::bind(sigc::mem_fun(*this, &ConfWindow::onFpsChange), &fpsComboBox) );
  layoutGrid.attach( fpsComboBox, 1, 2, 1, 1 );
  
  
  // All the other existing options
  for( int i = 0; i < camSettings->cameraControls.size(); i++ ) {
    CameraSettings::CameraControl* control = &camSettings->cameraControls[i];
    
    Gtk::Label* lb = Gtk::manage( new Gtk::Label( "   " + control->name ) );
    layoutGrid.attach( *lb, 0, 3 + i, 1, 1 );
    
    switch( control->type )
    {
      case V4L2_CTRL_TYPE_INTEGER:
      {
	Gtk::Scale* scale = Gtk::manage( new Gtk::Scale( Gtk::ORIENTATION_HORIZONTAL ) );
	scale->set_range( control->minValue, control->maxValue );
	scale->set_increments( control->step, control->step );
	scale->set_value( control->value );
	scale->set_draw_value( false );
	
	layoutGrid.attach( *scale, 1, 3 + i, 1, 1 );
	scale->signal_change_value().connect( sigc::bind(sigc::mem_fun(*this, &ConfWindow::onScaleValueChange), i) );
	break;
      }
      
      case V4L2_CTRL_TYPE_BOOLEAN:
      {
	Gtk::CheckButton* cb = Gtk::manage( new Gtk::CheckButton() );
	cb->set_active( control->value );
	layoutGrid.attach( *cb, 1, 3 + i, 1, 1 );
	cb->signal_toggled().connect( sigc::bind(sigc::mem_fun(*this, &ConfWindow::onCheckButtonToggle), i, cb) );
	break;
      }
	
      case V4L2_CTRL_TYPE_MENU:
      {
	Gtk::ComboBoxText* combo = Gtk::manage( new Gtk::ComboBoxText() );
	for( int j = 0; j < control->menuItems.size(); j++ )
	  combo->append( control->menuItems[j] );
	
	combo->set_active( control->value );
	layoutGrid.attach( *combo, 1, 3 + i, 1, 1 );
	combo->signal_changed().connect( sigc::bind(sigc::mem_fun(*this, &ConfWindow::onComboBoxValueChange), i, combo) );
	break;
      }
	
      default:
	printf( "Warning: unimplemented camera control type, configure window does not know how to display it. Type = %d\n", control->type );
    }

  }
  
}


void ConfWindow::onFormatChange( Gtk::ComboBoxText* comboBox )
{
  int id = comboBox->get_active_row_number();
  
  int res = camSettings->camera->setPixelFormat( camSettings->supportedFormats[id].name );
  if( res == 0 )
    *newImageRequired = ImageRequest;
  else
    printf( "Failed to change pixel format.\n" );
}


void ConfWindow::onResolutionChange( Gtk::ComboBoxText* comboBox )
{
  int width, height;
  Glib::ustring resStr = comboBox->get_active_text();
  
  sscanf( resStr.c_str(), "%d x %d", &width, &height );
  
  int res = camSettings->camera->setResolution( width, height );
  if( res == 0 )
    *newImageRequired = ImageRequest;
  else
    printf( "Failed to set resolution. %d: %s\n", errno, strerror(errno) );
}



void ConfWindow::onFpsChange( Gtk::ComboBoxText* comboBox )
{
  int newFPS = atoi( comboBox->get_active_text().c_str() );
  
  int res = camSettings->camera->setFPS( newFPS );
  if( res != 0 )
    printf( "Failed to set frame rate.\n" );
}


bool ConfWindow::onScaleValueChange( Gtk::ScrollType scroll, double newValue, int id )
{
  int correctValue = newValue;
  
  // Gtk::Scale behaves weirdly and gives values that are off its limits when moved with the mouse
  // For some reason the min-max values only work for keyboard input
  if( correctValue > camSettings->cameraControls[id].maxValue )
    correctValue = camSettings->cameraControls[id].maxValue;
  else if( correctValue < camSettings->cameraControls[id].minValue )
    correctValue = camSettings->cameraControls[id].minValue;
  
  camSettings->camera->setCameraControl( camSettings->cameraControls[id], correctValue );
  
  return false;
}

void ConfWindow::onCheckButtonToggle( int id, Gtk::CheckButton* cb )
{
  camSettings->camera->setCameraControl( camSettings->cameraControls[id], cb->get_active() );
}

void ConfWindow::onComboBoxValueChange( int id, Gtk::ComboBoxText* combo )
{
  camSettings->camera->setCameraControl( camSettings->cameraControls[id], combo->get_active_row_number() );
}



Glib::ustring ConfWindow::int2ustring( int val )
{
    std::ostringstream ssIn;
    ssIn << val;
    Glib::ustring strOut = ssIn.str();

    return strOut;
}














