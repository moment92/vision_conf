#ifndef CONF_WINDOW_H_INCLUDED
#define CONF_WINDOW_H_INCLUDED

#include "cameraSettings.h"

#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/label.h>
#include <gtkmm/comboboxtext.h>

namespace Gtk {
  class CheckButton;
}


enum NewImageRequest {
  NoRequest,
  ResizeWindowRequest,
  ImageRequest
};


class ConfWindow : public Gtk::Window
{
public:
  ConfWindow( CameraSettings* camSettings_, volatile NewImageRequest* newImgReq );
  ~ConfWindow( void );
  
  
private:
  void initWindow( void );
  void createWindow( void );
  
  void onFormatChange( Gtk::ComboBoxText* comboBox );
  void onResolutionChange( Gtk::ComboBoxText* comboBox );
  void onFpsChange( Gtk::ComboBoxText* comboBox );
  bool onScaleValueChange( Gtk::ScrollType scroll, double newValue, int id );
  void onCheckButtonToggle( int id, Gtk::CheckButton* cb );
  void onComboBoxValueChange( int id, Gtk::ComboBoxText* combo );
  
  Glib::ustring int2ustring( int val );
  
  
private:
  CameraSettings* camSettings;
  volatile NewImageRequest* newImageRequired;
  
  Gtk::Grid layoutGrid;
  
  Gtk::Label resolutionLabel;
  Gtk::Label fpsLabel;
  Gtk::Label fmtLabel;
  
  Gtk::ComboBoxText pixelFmtComboBox;
  Gtk::ComboBoxText resolutionComboBox;
  Gtk::ComboBoxText fpsComboBox;
};


#endif