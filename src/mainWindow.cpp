#include "mainWindow.h"



MainWindow::MainWindow() : Window()
{
  initWindow();
}



MainWindow::~MainWindow()
{
}



void MainWindow::initWindow()
{
  set_size_request( 600, 200 );
  
  createWindow();
  
  show_all_children();
}


void MainWindow::createWindow()
{ 
  // Widget container
  layoutGrid.set_row_homogeneous( true );
  layoutGrid.set_column_homogeneous( true );
  add( layoutGrid );
  
  
  // Combobox for selecting the color
  colorComboBox.append( "Orange" );
  colorComboBox.append( "Green" );
  colorComboBox.append( "Blue" );
  colorComboBox.append( "Yellow" );
  colorComboBox.append( "White" );
  colorComboBox.append( "Black" );

  colorComboBox.set_active( 0 );
  layoutGrid.attach( colorComboBox, 0, 0, 6, 1 );
  
  
  // Buttons
  confButton.set_label( "Configure" );
  resetButton.set_label( "Reset" );
  
  layoutGrid.attach( confButton, 9, 0, 3, 1 );
  layoutGrid.attach( resetButton, 9, 1, 3, 1 );
  
  
  // Checkbuttons
  detectBallCheckButton.set_label( "Detect Balls" );
  detectGateCheckButton.set_label( "Detect Gates" );
  
  layoutGrid.attach( detectBallCheckButton, 6, 0, 3, 1 );
  layoutGrid.attach( detectGateCheckButton, 6, 1, 3, 1 );
  
  
  // Labels
  frameBrushLabel.set_label( "Frame Brushsize" );
  colorBrushLabel.set_label( "Colorspace Brushsize" );
  thresFrameBrushLabel.set_label( "Thres. Frame Brushs." );
  thresColorBrushLabel.set_label( "Thres. Colors. Brushs." );
  
  setLabelAlignment();
  
  layoutGrid.attach( frameBrushLabel, 0, 2, 3, 1 );
  layoutGrid.attach( colorBrushLabel, 0, 3, 3, 1 );
  layoutGrid.attach( thresFrameBrushLabel, 6, 2, 3, 1 );
  layoutGrid.attach( thresColorBrushLabel, 6, 3, 3, 1 );

  
  // Text Entries
  frameBrushEntry.set_text( "5" );
  colorBrushEntry.set_text( "5" );
  thresFrameBrushEntry.set_text( "1" );
  thresColorBrushEntry.set_text( "1" );
  
  layoutGrid.attach( frameBrushEntry, 3, 2, 3, 1 );
  layoutGrid.attach( colorBrushEntry, 3, 3, 3, 1 );
  layoutGrid.attach( thresFrameBrushEntry, 9, 2, 3, 1 );
  layoutGrid.attach( thresColorBrushEntry, 9, 3, 3, 1 );
}


void MainWindow::setLabelAlignment()
{
  frameBrushLabel.set_alignment( Gtk::ALIGN_START );
  frameBrushLabel.set_margin_left( 10 );
  colorBrushLabel.set_alignment( Gtk::ALIGN_START );
  colorBrushLabel.set_margin_left( 10 );
  thresFrameBrushLabel.set_alignment( Gtk::ALIGN_START );
  thresFrameBrushLabel.set_margin_left( 10 );
  thresColorBrushLabel.set_alignment( Gtk::ALIGN_START );
  thresColorBrushLabel.set_margin_left( 10 );
}





