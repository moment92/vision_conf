#ifndef MAIN_WINDOW_H_INCLUDED
#define MAIN_WINDOW_H_INCLUDED

#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>



class MainWindow : public Gtk::Window
{
public:
  MainWindow( void );
  ~MainWindow( void );
  
  
private:
  void initWindow( void );
  void createWindow( void );
  void setLabelAlignment( void );

  
public:
  Gtk::Entry frameBrushEntry;
  Gtk::Entry colorBrushEntry;
  Gtk::Entry thresFrameBrushEntry;
  Gtk::Entry thresColorBrushEntry;
  
  Gtk::ComboBoxText colorComboBox;
  
  Gtk::Button confButton;
  Gtk::Button resetButton;
  
private:
  Gtk::Grid layoutGrid;

  Gtk::CheckButton detectBallCheckButton;
  Gtk::CheckButton detectGateCheckButton;
  
  Gtk::Label frameBrushLabel;
  Gtk::Label colorBrushLabel;
  Gtk::Label thresFrameBrushLabel;
  Gtk::Label thresColorBrushLabel;
};


#endif