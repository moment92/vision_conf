#include "displayWindow.h"

#include <X11/Xutil.h>
#include <sys/shm.h>
#include <cstdio>
#include <cstring>



DisplayWindow::DisplayWindow( const char* wndName, int width_, int height_, int xPos, int yPos )
  : width( width_ ),
    height( height_ ),
    displayReady( false ),
    img( NULL )
{
  display = XOpenDisplay( NULL );
  if( !display ) {
    printf( "Error: XOpenDisplay failed.\n" );
    return;
  }
  
  wmDeleteMessage = XInternAtom( display, "WM_DELETE_WINDOW", False );

  screen = XScreenOfDisplay( display, DefaultScreen(display) );
  
  window = XCreateSimpleWindow( display,
				RootWindowOfScreen(screen),
				0,
				0,
				width,
				height,
				0,
				BlackPixelOfScreen(screen),
				BlackPixelOfScreen(screen) );
  
  int res = imageCreate();
  if( res != 0 ) {
    printf( "Error: imageCreate() failed\n" );
    return;
  }
  
  XSelectInput( display, window, ButtonPressMask | KeyPressMask );
  XSetWMProtocols( display, window, &wmDeleteMessage, 1 );
  
  XMapRaised( display, window );
  XStoreName( display, window, wndName );
  
  setPosition( xPos, yPos );
  
  displayReady = true;
  
}



DisplayWindow::~DisplayWindow()
{
  if( displayReady )
    destroyWindow();
}



void DisplayWindow::run()
{
  XEvent event;
  
  if( !displayReady ) {
    printf( "DisplayWindow is not ready for running.\n" );
    return;
  }
  
  if( !img->isFrameSet() )
    return;

  showImage();
  
  if (XPending( display ) > 0) {
    XNextEvent( display, &event );
    
    if (event.type == ClientMessage && event.xclient.data.l[0] == (long) wmDeleteMessage) {
      destroyWindow();
    }
    
    if (event.type == ButtonPress) {
      if (!onButtonPress.empty()) {
	onButtonPress( event.xbutton.x, event.xbutton.y );
      }
    }
    
    if (event.type == KeyPress) {
      onKeyPress( event.xkey.keycode );
    }
  }
}



bool DisplayWindow::showImage()
{
  if( !displayReady ) {
    printf( "Error. showImage can't proceed, because the window is not properly initialized.\n" );
    return false;
  }
  
  lockDisplay();
  
  if( img && img->isFrameSet() ) {
    displayImage();
    img->displayed = true;
  }
  unLockDisplay();
  

  bool res = XShmPutImage( display, window, graphicsContext, xImage, 0, 0, 0, 0, width, height, false );
  if( !res ) {
    printf( "Error. Failed to show the image.\n" );
    return false;
  }

  return true;
}




int DisplayWindow::imageCreate()
{
  XGCValues gcValues;
  gcValues.function = GXcopy;
  ulong gcValuesMask = GCFunction;
  
  graphicsContext = XCreateGC( display, window, gcValuesMask, &gcValues );
  
  XWindowAttributes  windowAttributes;
  XGetWindowAttributes( display, window, &windowAttributes );
  
  Visual* visual = windowAttributes.visual;
  int depth = windowAttributes.depth;
  
  shmInfo.shmid = -1;
  shmInfo.shmaddr = NULL;
  
  xImage = XShmCreateImage( display, visual, depth, ZPixmap, NULL, &shmInfo, width, height );
  if( !xImage ) {
    printf( "Error. Failed to create image.\n" );
    return -1;
  }
  
  shmInfo.shmid = shmget( IPC_PRIVATE, xImage->bytes_per_line * xImage->height, IPC_CREAT | 0777 );
  if( shmInfo.shmid < 0 ) {
    printf( "Error. Failed to create a segment.\n" );
    return -1;
  }
  
  shmInfo.shmaddr = (char*) shmat( shmInfo.shmid, 0, 0 );
  if( shmInfo.shmaddr < 0 ) {
    printf( "Error. Failed to attach shared memory segment.\n" );
    return -1;
  }
  
  xImage->data = shmInfo.shmaddr;
  shmInfo.readOnly = false;
  
  bool res = XShmAttach( display, &shmInfo );
  if( !res ) {
    printf( "Error. Failed to attach shared memory to X window server.\n" );
    return -1;
  }
  
  displayData = (uchar*) xImage->data;
  
  return 0;
}



void DisplayWindow::destroyWindow()
{
  displayReady = false;
  
  if( xImage == NULL )
    return;
  
  if( shmInfo.shmid >= 0 ) {
    XShmDetach( display, &shmInfo );
    shmdt( shmInfo.shmaddr );
    shmInfo.shmaddr = NULL;
    shmctl( shmInfo.shmid, IPC_RMID, 0 );
    shmInfo.shmid = -1;
  }
  
  xImage->data = NULL;
  XDestroyImage( xImage );
  xImage = NULL;
  
  if( display != NULL ) {
    XFreeGC( display, graphicsContext );
    XDestroyWindow( display, window );
    display = NULL;
  }
}




void DisplayWindow::resizeWindow( int width_, int height_ )
{
  width = width_;
  height = height_;
  
  XResizeWindow( display, window, width, height );
  imageCreate();
}



void DisplayWindow::setImage( Image* img_ )
{
  img = img_;
}



void DisplayWindow::displayImage()
{
  uchar r, g, b;
  int counter = 0;
  
  for( int y = 0; y < height; y++ ) {
    for( int x = 0; x < width; x++ ) {
      
      img->getRGB( x, y, r, g, b );
      
      displayData[ counter ] = b;
      displayData[ counter + 1 ] = g;
      displayData[ counter + 2 ] = r;
      displayData[ counter + 3 ] = 255;		// Alpha channel
      
      counter += 4;
    }
  }
  
}



void DisplayWindow::lockDisplay()
{
  displayMutex.lock();
}


void DisplayWindow::unLockDisplay()
{
  displayMutex.unlock();
}

bool DisplayWindow::isDisplayLocked()
{
  return displayMutex.try_lock();
}




void DisplayWindow::setKeyPressHandler( sigc::slot<void, int> handler )
{
  onKeyPress = handler;
}


void DisplayWindow::setButtonPressHandler( sigc::slot<void, int, int> handler )
{
  onButtonPress = handler;
}


void DisplayWindow::getPosition( int& x, int& y )
{
  XTranslateCoordinates( display, window, RootWindowOfScreen(screen), 0, 0, &x, &y, new Window );
  
  // HACK: Not sure why this is needed. Maybe it is the height of the taskbar and width of the window frame
  x -= 1;
  y -= 22;
}


void DisplayWindow::setPosition( int x, int y )
{
  XMoveWindow( display, window, x, y );
}






