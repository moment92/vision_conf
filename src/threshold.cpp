#include "threshold.h"

#include <fstream>
#include <algorithm>
#include <cstring>

using namespace std;



Threshold::Threshold()
  : addColorBrush( 0 ),
    removeColorBrush( 0 )
{
  memset( thresData, 0, THRES_DATA_SIZE );
}



Threshold::~Threshold()
{
}




int Threshold::loadThreshold( string fileName )
{  
  ifstream f;
  f.open( fileName, ios_base::binary );
  
  if (f.good())
    f.read( (char*) thresData, THRES_DATA_SIZE );
  else
    return -1;
  
  f.close();
  makeBackUp();
  
  return 0;
}


int Threshold::saveThreshold( string fileName )
{
  ofstream f;
  f.open( fileName, ios_base::binary );
  
  if (f.good())
    f.write( (char*) thresData, THRES_DATA_SIZE );
  else
    return -1;

  f.close();
  
  return 0;
}



void Threshold::assignColorRange( int col1, int col2, int col3, uchar color )
{ 
  for (int i = col1 - addColorBrush; i <= col1 + addColorBrush; i++) {
    if (i < 0 || i > 255)
      continue;
    for (int j = col2 - addColorBrush; j <= col2 + addColorBrush; j++) {
      if (j < 0 || j > 255)
	continue;
      for (int k = col3 - addColorBrush; k <= col3 + addColorBrush; k++) {
	if (k < 0 || k > 255)
	  continue;
	
	if (isThresholdColor(i, j, k, 0))
	  assignColor( i, j, k, color );
      }
    }
  }
}


void Threshold::removeColorRange( int col1, int col2, int col3, uchar color )
{
  for (int i = col1 - removeColorBrush; i <= col1 + removeColorBrush; i++) {
    if (i < 0 || i > 255)
      continue;
    for (int j = col2 - removeColorBrush; j <= col2 + removeColorBrush; j++) {
      if (j < 0 || j > 255)
	continue;
      for (int k = col3 - removeColorBrush; k <= col3 + removeColorBrush; k++) {
	if (k < 0 || k > 255)
	  continue;
	
	if (isThresholdColor(i, j, k, color))
	  assignColor( i, j, k, 0 );
      }
    }
  }
}



void Threshold::setAddColorBrush( int size )
{
  addColorBrush = size;
}


void Threshold::setRemoveColorBrush( int size )
{
  removeColorBrush = size;
}


void Threshold::resetThreshold( uchar color )
{
  for (int col1 = 0; col1 < 256; col1++) {
    for (int col2 = 0; col2 < 256; col2++) {
      for (int col3 = 0; col3 < 256; col3++) {
	if (isThresholdColor(col1, col2, col3, color))
	  assignColor( col1, col2, col3, 0 );
      }
    }
  }
}


void Threshold::makeBackUp( void )
{
  memcpy( thresDataBackUp, thresData, THRES_DATA_SIZE );
}


void Threshold::resetToBackUp( void )
{
  memcpy( thresData, thresDataBackUp, THRES_DATA_SIZE );
}









