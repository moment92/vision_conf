#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

#include "mainWindow.h"
#include "confWindow.h"
#include "displayWindow.h"
#include "image.h"
#include "thresholdImage.h"

#include <gtkmm/application.h>


class Config;


class Gui
{
public:
  Gui( Image* img1, ThresholdImage* img2, CameraSettings* camSettings, volatile NewImageRequest* newImgReq, Config* config );
  ~Gui( void );
  
  void run( void );
  
  void lockDisplay( void );
  void unLockDisplay( void );
  
  void setNewImage( Image* img );
  void setNewThresImage( ThresholdImage* img );
  void resizeDisplayWindows( int width, int height );
  
  
private:
  bool onIdle( void );
  void closeApplication( void );
  
  void showConfWindow( void );
  
  void onKeyPress( int key );
  void onColorChange( void );
  void onFrameBrushChange( void );
  void onColorBrushChange( void );
  void onThresFrameBrushChange( void );
  void onThresColorBrushChange( void );
  
  
public:
  volatile bool running;
  volatile bool halt;
  volatile int exitCode;
  

private:
  Glib::RefPtr<Gtk::Application> application;
  MainWindow* mainWindow;
  ConfWindow* confWindow;
  DisplayWindow* videoWindow;
  DisplayWindow* thresWindow;
  
  Image* image;
  ThresholdImage* thresImage;
  
  volatile NewImageRequest* newImgRequired;
  
  Config* config;
  CameraSettings* cameraSettings;
};


#endif