#include "gui.h"
#include "camera.h"
#include "config.h"

#include "dummyImage.h"
#include "dummyThresImage.h"
#include "yuyvImage.h"
#include "yuyvImageThres.h"

#include <thread>
#include <mutex>


bool finished = false;
std::mutex finishedMutex;

volatile NewImageRequest newImgReq = NoRequest;

Image* img1 = NULL;
ThresholdImage* img2 = NULL;

Gui* pGui = NULL;
CameraSettings* camSettings;

Config config;
Threshold threshold;


void cameraWork( Camera* camera )
{
  bool exitNow = false;
  
  while (1) {
    uchar* frame = NULL;
    
    if (!pGui->halt)
      frame = camera->readFrame();
      
    if (frame != NULL) {
      pGui->lockDisplay();
	
      // When the format or resolution was dynamically changed
      if (newImgReq == ImageRequest) {
	delete img1;
	delete img2;
	
	if (camSettings->pixelFormat == "YUYV") {
	  img1 = new YuyvImage( camSettings->width, camSettings->height );
	  img2 = new YuyvImageThres( camSettings->width, camSettings->height, &threshold );
	} else {
	  img1 = new DummyImage( camSettings->width, camSettings->height );
	  img2 = new DummyThresImage( camSettings->width, camSettings->height, &threshold );
	}
	pGui->setNewImage( img1 );
	pGui->setNewThresImage( img2 );

	newImgReq = ResizeWindowRequest;
	
	pGui->unLockDisplay();
	continue;
      }

      
      img1->setFrame( frame );
      img2->setFrame( frame );
      
      pGui->unLockDisplay();
    }
    
    
    finishedMutex.lock();
    if (finished)
      exitNow = true;
    finishedMutex.unlock();
    
    if (exitNow)
      break;
  }
}



int main()
{
  int res;
  
  res = threshold.loadThreshold( config.thresPath );
  if (res != 0) {
    printf( "Failed to load threshold file (thresPath: %s)\n", config.thresPath.c_str() );
    printf( "Creating a new empty threshold file: %s\n", config.thresPath.c_str() );
    threshold.saveThreshold( config.thresPath );
  }
  
  Camera camera( config.videoDevice, &config.cameraConfig );
  res = camera.startCamera();
  
  if (res != 0) {
    printf( "Failed to start camera (videoDevice: %s)\n", config.videoDevice.c_str() );
    return res;
  }
  camSettings = &camera.cameraSettings;
  
  if (camSettings->pixelFormat == "YUYV") {
    img1 = new YuyvImage( camSettings->width, camSettings->height );
    img2 = new YuyvImageThres( camSettings->width, camSettings->height, &threshold );
  } else {
    img1 = new DummyImage( camSettings->width, camSettings->height );
    img2 = new DummyThresImage( camSettings->width, camSettings->height, &threshold );
  }
  
  
  Gui gui( img1, img2, camSettings, &newImgReq, &config );
  pGui = &gui;
  
  
  std::thread cameraWorker( cameraWork, &camera );
  gui.run();  
  
  if (gui.exitCode == -1) {
    printf( "Failed to run GUI.\n" );
    return gui.exitCode;
  }
  
  finishedMutex.lock();
  finished = true;
  finishedMutex.unlock();
  
  cameraWorker.join();
  
  delete img1;
  delete img2;
  
  
  // Saving data
  threshold.saveThreshold( config.thresPath );
  config.applyCameraSettings( camSettings );
  config.createConfig();
  
  return 0;
}