#include "config.h"

#include <fstream>
#include <sstream>
#include <boost/algorithm/string.hpp>

using namespace std;


Config::Config()
{
  setDefaultConfig();
  
  int res = readConfig();
  if (res != 0) {
    printf( "vconf file is missing. Creating a new one. Using default settings.\n" );
    createConfig();
  }
}


Config::~Config()
{
}



int Config::readConfig()
{
  ifstream f;
  string line;
  
  f.open( "vconf" );
  
  if (f.good()) {
    while (!f.eof()) {
      getline( f, line );
      parseConfigLine( line );
    }
  } else
    return -1;
  
  f.close();
  return 0;
}


void Config::parseConfigLine( string& line )
{
  // lines starting with '#' are comments and should be ignored
  for (int i = 0; i < line.length(); i++) {
    if( line[i] == ' ' )
      continue;
    if( line[i] == '#' )
      return;
    break;
  }
  
  vector<string> tempParts, parts;
  boost::split( tempParts, line, boost::is_any_of("\t: ") );
  for (int i = 0; i < tempParts.size(); i++) {
    if (tempParts[i] != "")
      parts.push_back( tempParts[i] );
  }
  
  if( parts.size() < 2 )
    return;
  
  interpretContents( parts );
}



void Config::interpretContents( vector<string>& parts )
{ 
  if (parts[0] == "thresPath")
    thresPath = connectParts( parts, 1 );
  if (parts[0] == "videoDevice")
    videoDevice = parts[1];
  
  if (parts[0] == "wndCoords") {
    if (parts.size() == 9) {
      mainWnd_x = stoi( parts[1] );
      mainWnd_y = stoi( parts[2] );
      confWnd_x = stoi( parts[3] );
      confWnd_y = stoi( parts[4] );
      wnd1_x = stoi( parts[5] );
      wnd1_y = stoi( parts[6] );
      wnd2_x = stoi( parts[7] );
      wnd2_y = stoi( parts[8] );
    }
  }
  
  if (parts[0] == "cameraControl") {
    if (parts.size() == 3)
      interpretCameraControl( parts[1], parts[2] );
  }
}


void Config::interpretCameraControl( string control, string value )
{
  if (control == "fps")
    cameraConfig.fps = stoi( value );
  else if (control == "width")
    cameraConfig.width = stoi( value );
  else if (control == "height")
    cameraConfig.height = stoi( value );
  else if (control == "pixelFormat")
    cameraConfig.pixelFormat = value;
  else {
    CameraConfig::ControlPair ctrl;
    ctrl.name = control;
    ctrl.value = stoi( value );
    cameraConfig.controls.push_back( ctrl );
  }
  
  if (!cameraConfig.cameraConfigExists) {
    cameraConfig.cameraConfigExists = true;
    setDefaultCameraConfig();
  }
}



void Config::createConfig()
{
  ofstream f;
  char coords[100];
  
  f.open( "vconf" );
  
  if (f.good()) {
    f << "#\n";
    f << "# vconf file is used for keeping settings for vision_conf\n";
    f << "#\n\n";
    
    f << "# The camera device\n";
    f << "videoDevice: " + videoDevice + "\n\n";
    
    f << "# Path to the threshold file\n";
    f << "thresPath: " + thresPath + "\n\n";
    
    f << "# Initial window coordinates of mainWindow, confWindow, videoWindow, thresholdWindow\n";
    sprintf( coords, "%d %d %d %d %d %d %d %d", mainWnd_x, mainWnd_y, confWnd_x, confWnd_y, wnd1_x, wnd1_y, wnd2_x, wnd2_y );
    f << "wndCoords: " + string(coords) + "\n\n";
    
    f << "# Camera controls\n";
    f << "cameraControl: fps " + int2str(cameraConfig.fps) + "\n";
    f << "cameraControl: width " + int2str(cameraConfig.width) + "\n";
    f << "cameraControl: height " + int2str(cameraConfig.height) + "\n";
    f << "cameraControl: pixelFormat " + cameraConfig.pixelFormat + "\n";
    
    for (int i = 0; i < cameraConfig.controls.size(); i++) {
      CameraConfig::ControlPair ctrl = cameraConfig.controls[i];
      f << "cameraControl: " + ctrl.name + " " + int2str(ctrl.value) + "\n";
    }
    
  } else {
    printf( "Failed to create a vconf file.\n" );
    return;
  }
  
  f.close();
}


void Config::setDefaultConfig()
{
  videoDevice = "/dev/video0";
  thresPath = "colorTable";
  
  mainWnd_x = 340;
  mainWnd_y = 50;
  confWnd_x = 300;
  confWnd_y = 200;
  wnd1_x = 100;
  wnd1_y = 300;
  wnd2_x = 500;
  wnd2_y = 300;
}


void Config::setDefaultCameraConfig()
{
  cameraConfig.fps = 30;
  cameraConfig.width = 640;
  cameraConfig.height = 480;
  cameraConfig.pixelFormat = "default";
}



void Config::applyCameraSettings( CameraSettings* settings )
{
  cameraConfig.fps = settings->fps;
  cameraConfig.pixelFormat = settings->pixelFormat;
  cameraConfig.width = settings->width;
  cameraConfig.height = settings->height;
  
  cameraConfig.controls.clear();
  
  for (int i = 0; i < settings->cameraControls.size(); i++) {
    CameraConfig::ControlPair ctrl;
    ctrl.name = settings->cameraControls[i].varName;
    ctrl.value = settings->cameraControls[i].value;
    
    cameraConfig.controls.push_back( ctrl );
  }
}



string Config::connectParts( vector<string>& parts, int startId )
{
  string connectedStr = "";
  
  for (int i = startId; i < parts.size(); i++) {
    if( i != startId )
      connectedStr += " ";
    
    connectedStr += parts[i];
  }
  return connectedStr;
}

string Config::int2str( int val )
{
  stringstream ss;
  ss << val;
  return ss.str();
}





